# Crypto Kait Crypto Challenge #1

## Questions

Our officers have obtained password dumps storing hacker passwords. After obtaining a few plaintext passwords, it appears that they are all encoded using different number
bases.

1. (15 points) 01000100 00110001 01110110 00110001 01101110 00110001 01110100 01111001
2. (15 points) 102 060 162 144 063 162 154 100 156 144 163
3. (15 points) 83 107 121 114 105 109
4. (15 points) 0x426174746c334672306e744949
5. (15 points) UGF0aCAwZiBFeGlsZQ==

## Hint: https://gchq.github.io/CyberChef/

## Solutions

1. D1v1n1ty (binary)
2. B0rd3rl@nds (octal)
3. Skyrim (decimal)
4. Battl3Fr0ntII (hex)
5. Path 0f Exile (base64)