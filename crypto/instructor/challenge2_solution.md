# Crypto Kait Crypto Challenge #2

## Questions

Our officers have obtained password dumps storing hacker passwords. After obtaining a
few plaintext passwords, it appears that some sort of shift cipher was used.

1. (20 points) APY-FUVSG-4237
2. (20 points) UJS-ZOPMA-8931

## Hint: https://gchq.github.io/CyberChef/

## Solutions
1. NCL-SHIFT-4237 (ROT13)
2. NCL-SHIFT-8931 (ROT19)