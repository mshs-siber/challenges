# Crypto Kait Crypto Challenge #3

## Questions
1. (25 points) 
.-.. .. ..-. .
.. ...
.-- .... .- -
.... .- .--. .--. . -. ...
.-- .... . -.
-.-- --- ..- .----. .-. .
-... ..- ... -.--
-- .- -.- .. -. --.
--- - .... . .-.
.--. .-.. .- -. ...

## Hint: https://gchq.github.io/CyberChef/

## Solutions
1. LIFE IS WHAT HAPPENS WHEN YOU'RE BUSY MAKING OTHER PLANS