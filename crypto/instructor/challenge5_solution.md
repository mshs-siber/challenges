# Crypto Kait Crypto Challenge #5

## Questions

Our officers have obtained an encrypted message. The forensics team was able to find a file that contains the string, "private" which was used to encrypt the message. Take it from here and obtain the plaintext message.

1. (50 points) Wrkfs4Iyej zzqnihkmy smeila ppweiv nmof @peyi1. Io kiefvne. Brxkqvtx Tarv W.

## Hint: https://gchq.github.io/CyberChef/

## Solutions

1. Hacks4Pups requested status update from @lpha1. No reponse. Initiate Plan B. 
(vigenere decode)