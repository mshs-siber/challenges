# Crypto Kait Network Analysis Challenge - FTP

Use the provided packet capture to answer these questions about FTP traffic.
(use file: NCL-NetworkTraffic-FTP.pcap or goo.gl/FJv6BS)

## Questions

1. (20 points) What was the first username/password combination attempt made to log in
to the server? ex. 'user/password'
2. (20 points) What software is the FTP server running? (Include name and version)
3. (20 points) What is the first username/password combination that allows for successful authentication? ex. 'user/password'
4. (20 points) What is the first command the user executes on the ftp server?
5. (20 points) What file is deleted from the ftp server?
6. (20 points) What file is uploaded to the ftp server?
7. (20 points) What is the MD5 sum of the uploaded file?
8. (20 points) What file does the anonymous user download?

## Solutions

1. user1/cyberskyline
2. FileZilla Server 0.9.53 beta
3. user1/metropolis
4. LIST
5. bank.cap
6. compcodes.zip
7. 3303628e25d43be4e11cc8878c5c5878 (carve file, then hash)
8. compcodes.zip