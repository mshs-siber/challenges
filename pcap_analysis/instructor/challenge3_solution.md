# Crypto Kait Network Analysis Challenge - HTTP

Use the provided capture to answer the following questions about a HTTP download.
(use file: NCL-HTTP.pcap or goo.gl/yF1WyN)

## Questions

1. (20 points) What Linux tool was used to execute a file download?
2. (20 points) What is the name of the web server software that handled the request?
3. (20 points) What IP address initiated request?
4. (20 points) What is the IP address of the server?
5. (20 points) What is the md5sum of the file downloaded?

## Solutions

1. Wget
2. nginx
3. 192.168.1.140
4. 174.143.213.184
5. 966007c476e0c200fba8b28b250a6379