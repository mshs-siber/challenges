# Crypto Kait Network Analysis Challenge - DNS

Use the provided packet capture to answer these questions about DNS traffic.
(use file: NCL-NetworkTraffic-DNS.pcap or goo.gl/nBSKWE)

## Questions

1. (20 points) What is the type of the DNS query requested?
2. (20 points) What domain was requested?
3. (20 points) How many items were in the response?
4. (20 points) What is the TTL for all of the records?
5. (20 points) What is the IP address for the "welcome" subdomain?

## Solutions

1. AXFR
2. etas.com
3. 4
4. 3600
5. 1.1.1.1