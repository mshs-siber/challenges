# Crypto Kait OSINT Challenge #2

## Questions
1. (15 points) What is the recipient’s email address?
2. (15 points) What is the sender’s email address?
3. (15 points) What IP address retrieves the email? (nearest to recipient)
4. (15 points) What is the content type of the message?
5. (15 points) What version of MIME is being used?
6. (15 points) What day of the week was the message received?

## Answers
1. alpha1@hacknet.cityinthe.cloud
2. gh0st@hacknet.cityinthe.cloud
3. 177.75.184.96 
4. text/plain
5. 1.0
6. Wednesday