# Crypto Kait OSINT Challenge #1

## Questions
1. (25 points) What is the CVE of the original POODLE attack?
2. (25 points) What version of VSFTPD contained the smiley face backdoor?
3. (25 points) What was the first 1.0.1 version of OpenSSL that was NOT vulnerable to Heartbleed?
4. (25 points) What was the original RFC number that described Telnet?
5. (25 points) How large (in bytes) was the SQL Slammer worm?
6. (25 points) Samy is my...

## Answers
1. CVE-2014-3566
2. 2.3.4
3. 1.0.1g
4. 854
5. 376
6. worm