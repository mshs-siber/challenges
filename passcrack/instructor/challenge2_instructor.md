# Pass Crack Challenge #2

Our officers have obtained password dumps storing hacker passwords. It appears that they are all in the format: "SKY-KAIT-" followed by 4 digits. Can you crack them?

## Hashes
c38d29e8899455c85ee03d11abbd262b
ff8f9efad5c9f106ac39e5290d810c91
425206344bd204933a38236b715c498f
ab37c335e51b2855cb5a11ca89041733
82dcf30f8c7c8d4f23961f7e0c1d3cee

## Hint: Brute Force Mask Attack https://hashcat.net/wiki/doku.php?id=mask_attack

### Solution Invocation
```bash
hashcat -a 3 -m 0 -O kait2.hash SKY-KAIT-?d?d?d?d
```

## Answers
c38d29e8899455c85ee03d11abbd262b:SKY-KAIT-6823
ab37c335e51b2855cb5a11ca89041733:SKY-KAIT-4130
82dcf30f8c7c8d4f23961f7e0c1d3cee:SKY-KAIT-5892
ff8f9efad5c9f106ac39e5290d810c91:SKY-KAIT-9965
425206344bd204933a38236b715c498f:SKY-KAIT-3572