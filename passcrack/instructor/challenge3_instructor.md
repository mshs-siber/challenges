# Crypto Kait Pass Crack Challenge #3

## Hashes
3546576a03c2c8229175eede8c02f891
a19d7a52bff83b0e4012d2c766e2f731
5a31b6b31f92c8f797505ca26af4b9de
857875c031fce47b2d40be0ce3ffd0bf
dc6054fbe36c8a2bd49b1d05b3b872ee

## Hint 1: Pokemon Word List
https://gist.github.com/azai91/31e3b31cbd3992a1cc679017f850a022

## Hint 2: Wordlist + Rules-based attack
https://www.notsosecure.com/one-rule-to-rule-them-all/

### Solution Invocation
```bash
hashcat -m 0 -O kait3.hash pokenmon.wl -r rules/rockyou-30000.rule
```

## Answers
3546576a03c2c8229175eede8c02f891:Wartortle8
a19d7a52bff83b0e4012d2c766e2f731:Beedrill15
5a31b6b31f92c8f797505ca26af4b9de:Weezing110
857875c031fce47b2d40be0ce3ffd0bf:Milotic350
dc6054fbe36c8a2bd49b1d05b3b872ee:Absol359
