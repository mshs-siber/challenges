# Crypto Kait Pass Crack Challenge #1

## Hashes
8549137cd494c22ae87eef3e18a46986
0f96a320a8c0bf7e3f6d375b0d9d3a4c
1a8cb8d148b513dfa1d285077fc4e3fb
22a313110bf5b84c0a58eecc27deaa30
e4fd50109f0e40e8c1a895d8e5c71199

## Hint: 
MD5 dictionary attack

### Solution Invocation
```bash
hashcat -a 0 -m 0 -O kait.hash ../breach.wl
```

## Answers
8549137cd494c22ae87eef3e18a46986:chasity
0f96a320a8c0bf7e3f6d375b0d9d3a4c:hustler
1a8cb8d148b513dfa1d285077fc4e3fb:loved1
e4fd50109f0e40e8c1a895d8e5c71199:poppy123
22a313110bf5b84c0a58eecc27deaa30:trivium

